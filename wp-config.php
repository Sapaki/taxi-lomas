<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'taxi-lomas' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Zz0o0JZlFeTHrGEiVWisfnAQ0q5VARsMnlZwwJjuFsVXkScfyoCcXLEAVXnJKjYJ' );
define( 'SECURE_AUTH_KEY',  't9iDz9CUNsvC5McK2Mn8ugmlxEOMu62M7XvnVqBXpmdtbRNrIUtFjhBA3xtn4EFt' );
define( 'LOGGED_IN_KEY',    'grVldiGPLeurK4AsbFhEvWb2gSoUqRTOJSxSEqEL7p5qR19ZfJ4GBNpNLBWDf5Ro' );
define( 'NONCE_KEY',        'oSjOfA25jdMOUKy3k3hf9dt8PBypmR8PT0P4IMOUOnDF8z7R6UMVVkNTaqdz1sid' );
define( 'AUTH_SALT',        'c1hzhpWckE0emfcnWlK0jLTEIYU7KPEBWz55OJ4QuM1cdujxyB1q9dis9VjCTBSL' );
define( 'SECURE_AUTH_SALT', '0sCCVOebyI72TDLdngqw4NFBhSo9XBSgDV80vr5gcCr1Zt2GMFQOZyxr50SPwE1P' );
define( 'LOGGED_IN_SALT',   'C2LRldQJHVIWvJiFs7wciGr8F80ZRPqglwD1V9mJWwKwPrCSWcHhl4urbJYtPZHz' );
define( 'NONCE_SALT',       'iyXw0vwy05HrL9fRfoV5qOKL5aAkvhZCx4nXIrOVGzbtb2uMJOrBaKAzLUMQfEI5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
